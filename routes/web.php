<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\auth\GoogleController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\ItemsController;
use App\Http\Controllers\BranchesController;
use App\Http\Controllers\StocksController;
use App\Http\Controllers\TransactionsController;
use App\Http\Controllers\PermissionsController;
use App\Http\Controllers\CustomersController;
use App\Http\Controllers\FoodCategoryController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\CheckoutController;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/' );
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/about-us', [HomeController::class, 'aboutus'])->name('about-us');
Route::get('/services', [HomeController::class, 'services'])->name('services');
Route::get('/contact', [HomeController::class, 'contact'])->name('contact');
Route::get('/customer-menu', [HomeController::class, 'menu'])->name('customer-menu');

Auth::routes();
Route::get('auth/google', [GoogleController::class, 'redirectToGoogle']);
Route::get('auth/google/callback', [GoogleController::class, 'handleGoogleCallback']);


Route::group(['namespace' => '\App\Http\Controllers'], function () {

    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
   // Route::redirect('/', '/login')->name('home');
    // Permissions
    Route::delete('permissions/destroy', [PermissionsController::class, 'massDestroy'])->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Items
    Route::delete('items/destroy', 'ItemsController@massDestroy')->name('items.massDestroy');
    Route::resource('items', 'ItemsController');

    // Branches
    Route::delete('branches/destroy', 'BranchesController@massDestroy')->name('branches.massDestroy');
    Route::resource('branches', 'BranchesController');

    // Stocks
    //Route::delete('stocks/destroy', 'StocksController@massDestroy')->name('stocks.massDestroy');
    Route::resource('stocks', 'StocksController')->only(['index', 'show']);

    // Transactions
    // Route::delete('transactions/destroy', 'TransactionsController@massDestroy')->name('transactions.massDestroy');
    Route::post('transactions/{stock}/storeStock', 'TransactionsController@storeStock')->name('transactions.storeStock');
    Route::resource('transactions', 'TransactionsController')->only(['index']);

    //customers
    Route::resource('customers', 'CustomersController');

    //food categories
    Route::resource('foodcategories', 'FoodCategoryController');

    //menu
    Route::resource('menu', 'MenuController');

    //Orders
    Route::resource('orders', 'OrdersController');

});

//cart

    Route::get('cart', [CheckoutController::class, 'cart'])->name('cart');
    Route::get('add-to-cart/{id}', [CheckoutController::class, 'addToCart'])->name('add.to.cart');
    Route::post('update-cart', [CheckoutController::class, 'update'])->name('update.cart');
    Route::post('remove-from-cart', [CheckoutController::class, 'remove'])->name('remove.from.cart');
    Route::get('place-order', [CheckoutController::class, 'placeOrder'])->name('place-order');
    Route::post('checkout-order', [CheckoutController::class, 'checkoutOrder'])->name('checkout-order');

// Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
// // Change password
//     if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
//         Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
//         Route::post('password', 'ChangePasswordController@update')->name('password.update');
//     }

// });

