@extends('frontend.layouts.pages')

@section('content')


    <div class="container-xxl py-5 bg-dark hero-header mb-5">
                <div class="container text-center my-5 pt-5 pb-4">
                    <h1 class="display-3 text-white mb-3 animated slideInDown">Food Menu</h1>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb justify-content-center text-uppercase">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Pages</a></li>
                            <li class="breadcrumb-item text-white active" aria-current="page">Menu</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <!-- Navbar & Hero End -->


<!-- Menu Start -->
<div class="container-xxl py-5">
    <div class="container">
        <div class="text-center wow fadeInUp" data-wow-delay="0.1s">
            <h5 class="section-title ff-secondary text-center text-primary fw-normal">Food Menu</h5>
            <h1 class="mb-5">Most Popular Items</h1>
        </div>
        <div class="tab-class text-center wow fadeInUp" data-wow-delay="0.1s">
            <ul class="nav nav-pills d-inline-flex justify-content-center border-bottom mb-5">
                <li class="nav-item">
                    <a class="d-flex align-items-center text-start mx-3 ms-0 pb-3 active" data-bs-toggle="pill" href="#tab-1">
                        <i class="fa fa-coffee fa-2x text-primary"></i>
                        <div class="ps-3">
                            <small class="text-body">Popular</small>
                            <h6 class="mt-n1 mb-0">Breakfast</h6>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="d-flex align-items-center text-start mx-3 pb-3" data-bs-toggle="pill" href="#tab-2">
                        <i class="fa fa-hamburger fa-2x text-primary"></i>
                        <div class="ps-3">
                            <small class="text-body">Special</small>
                            <h6 class="mt-n1 mb-0">Launch</h6>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="d-flex align-items-center text-start mx-3 me-0 pb-3" data-bs-toggle="pill" href="#tab-3">
                        <i class="fa fa-utensils fa-2x text-primary"></i>
                        <div class="ps-3">
                            <small class="text-body">Lovely</small>
                            <h6 class="mt-n1 mb-0">Dinner</h6>
                        </div>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane fade show p-0 active">
                    <div class="row g-4">
                        <?php
                        foreach (session('cart') as $key => $value) {
                            $cart_items [] = $value['name'];
                        }

                        foreach($menu as $value){
                            if ($value['food_category_id'] == 1) {
                                
                                ?>
                                <div class="col-lg-6">
                                    <div class="d-flex align-items-center">
                                        <img class="flex-shrink-0 img-fluid menu-cover-img" src="{{ asset('assets/frontend/img/menu-1.jpg') }}" alt="">
                                        <div class="w-100 d-flex flex-column text-start ps-4">
                                            <h5 class="d-flex justify-content-between border-bottom pb-2">
                                                <span>{{ $value['name'] }}</span>
                                                <span class="text-primary">Ksh. {{$value['price'] }}</span>
                                                
                                                    <?php

                                                    if (array_search($value['name'], $cart_items) != '') {
                                                        foreach (session('cart') as $key => $val1) {
                                                            if ($value['name'] == $val1['name']) {
                                                                $quantity = $val1['quantity'];
                                                                $reduce_quantity = $quantity - 1;
                                                                ?>
                                                                <table>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <form class="needs-validation" novalidate action="{{ route('update.cart') }}" method="POST" id="cart-form">
                                                                                      @csrf
                                                                                    <input type="hidden" class="form-control" name="id" value="{{ $value['id'] }}">
                                                                                    <input type="hidden" class="form-control" name="quantity" value="{{ $reduce_quantity }}">

                                                                                    <button type="submit" class="btn btn-primary"> - </button>
                                                                                </form>
                                                                            </td>
                                                                            <td>
                                                                                <a class="btn btn-primary" href="{{ route('add.to.cart', $value->id) }}"> + </a>
                                                                            </td>
                                                                            <td>
                                                                                <span class="btn btn-primary">{{
                                                                                $quantity }}</span>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <?php
                                                            }
                                                        }
                                                        
                                                        
                                                    }
                                                    else
                                                    { 
                                                        ?>
                                                        <span class="text-primary"><a href="{{ route('add.to.cart', $value->id) }}">Order Now</a></span>

                                                        <?php
                                                    }
                                                ?>
                                                
                                            </h5>
                                            <small class="fst-italic">{{ $value['description'] }}</small>
                                        </div>
                                    </div>
                                </div> 
                               <?php
                           }
                        }
                        ?>                       
                    </div>
                </div>
                <div id="tab-2" class="tab-pane fade show p-0">
                    <div class="row g-4">
                        <?php 
                        foreach($menu as $value){
                            if ($value['food_category_id'] == 2) {
                                ?>
                                <div class="col-lg-6">
                                    <div class="d-flex align-items-center">
                                        <img class="flex-shrink-0 img-fluid rounded" src="{{ asset('assets/frontend/img/menu-1.jpg') }}" alt="" style="width: 80px;">
                                        <div class="w-100 d-flex flex-column text-start ps-4">
                                            <h5 class="d-flex justify-content-between border-bottom pb-2">
                                                <span>{{ $value['name'] }}</span>
                                                <span class="text-primary">Ksh. {{$value['price'] }}</span>
                                                <span class="text-primary"><a href="{{ route('add.to.cart', $value->id) }}">Order Now</a></span>
                                            </h5>
                                            <small class="fst-italic">{{ $value['description'] }}</small>
                                        </div>
                                    </div>
                                </div> 
                               <?php
                           }
                        }
                        ?>                       
                    </div>
                </div>
                <div id="tab-3" class="tab-pane fade show p-0">
                    <div class="row g-4">
                        <?php 
                        foreach($menu as $value){
                            if ($value['food_category_id'] == 3) {
                                ?>
                                <div class="col-lg-6">
                                    <div class="d-flex align-items-center">
                                        <img class="flex-shrink-0 img-fluid rounded" src="{{ asset('assets/frontend/img/menu-1.jpg') }}" alt="" style="width: 80px;">
                                        <div class="w-100 d-flex flex-column text-start ps-4">
                                            <h5 class="d-flex justify-content-between border-bottom pb-2">
                                                <span>{{ $value['name'] }}</span>
                                                <span class="text-primary">Ksh. {{$value['price'] }}</span>
                                                <span class="text-primary"><a href="{{ route('add.to.cart', $value->id) }}">Order Now</a></span>
                                            </h5>
                                            <small class="fst-italic">{{ $value['description'] }}</small>
                                        </div>
                                    </div>
                                </div> 
                               <?php
                           }
                        }
                        ?>                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Menu End -->
@endsection
