
@include('frontend.includes.head')
@include('frontend.includes.header')
@include('message.flash-message')
@yield('content')
@include('frontend.includes.footer')
