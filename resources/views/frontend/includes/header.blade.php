
<body>
    <div class="container-xxl bg-white p-0">
        <!-- Spinner Start -->
        <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
            <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
        <!-- Spinner End -->


        <!-- Navbar & Hero Start -->
        <div class="container-xxl position-relative p-0">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark px-4 px-lg-5 py-3 py-lg-0" id="flex div-navbar">
                    <a href="" class="navbar-brand p-0">
                        <h1 class="text-primary m-0"><i class="fa fa-utensils me-3"></i>BAITI</h1>
                        <!-- <img src="img/logo.png" alt="Logo"> -->
                    </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                        <span class="fa fa-bars"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarCollapse">
                        <div class="navbar-nav ms-auto py-0 pe-4">
                            <a href="{{ route('home') }}" class="nav-item nav-link active">Home</a>
                            <a href="{{ route('about-us') }}" class="nav-item nav-link">About</a>
                            <a href="{{ route('services') }}" class="nav-item nav-link">Service</a>
                            <a href="{{ route('customer-menu') }}" class="nav-item nav-link">Menu</a>
                            <a href="{{ route('contact') }}" class="nav-item nav-link">Contact</a>
                        </div>
                        <a href="{{ route('home') }}/#booking" class="btn btn-primary py-2 px-4 header-btns">Book A Table</a>
                            <div class="dropdown">
                                <button type="button" class="btn btn-primary py-2 px-4 header-btns" data-toggle="dropdown">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart <span class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>
                                </button>
                                
                                <div class="dropdown-menu">
                                    <div class="row total-header-section">
                                        <div class="col-lg-3 col-sm-3 col-3">
                                            <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>
                                        </div>
                                        @php $total = 0 @endphp
                                        @foreach((array) session('cart') as $id => $details)
                                            @php $total += $details['price'] * $details['quantity'] @endphp
                                        @endforeach
                                        <div class="col-lg-6 col-sm-6 col-6 total-section text-right">
                                            <p>Total: <span class="text-info">Ksh. {{ $total }}</span></p>
                                        </div>
                                    </div>
                                    @if(session('cart'))
                                        @foreach(session('cart') as $id => $details)
                                            <div class="row cart-detail">
                                                <div class="col-lg-3 col-sm-3 col-3 cart-detail-img">
                                                    <img src="{{ asset('assets/frontend/img/menu-1.jpg') }}" class="cart-img" />
                                                </div>
                                                <div class="col-lg-9 col-sm-9 col-9s cart-detail-product">
                                                    <p>{{ $details['name'] }}<br>
                                                    <span class="price text-info"> Ksh. {{ $details['price'] }}, </span> <span class="count"> Quantity: {{ $details['quantity'] }}</span>
                                                    </p>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                    <div class="row">
                                        <div class="col-lg-12 col-sm-12 col-12 text-center checkout">
                                            <a href="{{ route('cart') }}" class="btn btn-primary btn-block cart-btn" >View all</a>
                                        </div>
                                    </div>
                                </div>
                            
                            </div>  
                    </div>
                </nav>
            </div>
        </div>

<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="location"]').on('change', function() {
            var locationId = $(this).val();
            baseUrl = '{{ url('/') }}';
            if(locationId) {
                $.ajax({
                    url: baseUrl+'/restaurant-address/'+locationId,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        document.getElementById("street").innerHTML = data.street;
                        document.getElementById("contact-number").innerHTML = '+'+data.phone_number;
                        document.getElementById("email-address").innerHTML = data.email;
                        document.getElementById("phone-contact").innerHTML = '+'+data.phone_number;
                    }
                });
            }
        });
    });
</script>
