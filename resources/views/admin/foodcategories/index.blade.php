@extends('admin.layouts.admin')
@section('content')

<div class="row">
    <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
      <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0"><span>Users List</span></h5>
                <ol class="breadcrumbs mb-0">
                  <li class="breadcrumb-item"><a href="index.html">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="#">User</a>
                  </li>
                  <li class="breadcrumb-item active">Users List
                  </li>
                </ol>
              </div>
              @can('user_create')
              <div class="col s2 m6 l6"><a href="#add-foodcategory-modal" class="btn waves-effect waves-light breadcrumbs-btn right modal-trigger"><span class="hide-on-small-onl">{{ trans('global.add') }} Food Categories</span></a>                
              </div>
              @endcan
            </div>
          </div>
        </div>
        <div class="col s12">
        <div class="container">
        <!-- users list start -->
        <section class="users-list-wrapper section">
          <div class="users-list-filter">
            <div class="card-panel">
              <div class="row">
                <form>
                  <div class="col s12 m6 l3">
                    <label for="users-list-verified">Verified</label>
                    <div class="input-field">
                      <select class="form-control" id="users-list-verified">
                        <option value="">Any</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                      </select>
                    </div>
                  </div>
                  <div class="col s12 m6 l3">
                    <label for="users-list-role">Role</label>
                    <div class="input-field">
                      <select class="form-control" id="users-list-role">
                        <option value="">Any</option>
                        <option value="User">User</option>
                        <option value="Staff">Staff</option>
                      </select>
                    </div>
                  </div>
                  <div class="col s12 m6 l3">
                    <label for="users-list-status">Status</label>
                    <div class="input-field">
                      <select class="form-control" id="users-list-status">
                        <option value="">Any</option>
                        <option value="Active">Active</option>
                        <option value="Close">Close</option>
                        <option value="Banned">Banned</option>
                      </select>
                    </div>
                  </div>
                  <div class="col s12 m6 l3 display-flex align-items-center show-btn">
                    <button type="submit" class="btn btn-block indigo waves-effect waves-light">Show</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="users-list-table">
            <div class="card">
              <div class="card-content">
                <!-- datatable start -->
                <div class="responsive-table">
                  <table id="users-list-datatable" class="table">
                <thead>
                    <tr>
                      <th>
                        Id
                      </th>
                        <th>
                            Name
                        </th>
                        
                        <th>
                            {{ trans('cruds.item.fields.description') }}
                        </th>
                        <th>
                          Status
                        </th>
                        <th>
                          Edit
                        </th>
                        <th>
                          Delete
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($food_categories as $key => $category)
                        <tr data-entry-id="{{ $category->id }}">
                            <td>
                                {{ $category->id }}
                            </td>
                            <td>
                               {{ $category->name }}
                            </td>
                            <td>
                                {{ $category->description }}
                            </td>
                            <td>
                              <?php if ($category->status ==  1) {
                                echo "Active";
                              }
                              else{
                                echo "Inactive";
                              } ?>
                            </td>
                              <td>
                                @can('item_edit')
                                    <a  href="{{ route('foodcategories.edit', $category->id) }}">
                                        <i class="material-icons">edit</i>
                                    </a>
                                @endcan
                              </td>
                            
                            <td>

                                @can('item_delete')
                                    <form action="{{ route('foodcategories.destroy', $category->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
                </table>
                </div>
                <!-- datatable ends -->
              </div>
            </div>
          </div>
        </section>
      </div>
      <div class="content-overlay"></div>
    </div>
  </div>
</div>
@endsection

<div id="add-foodcategory-modal" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Add Customer</h4>
    <form method="POST" action="{{ route('foodcategories.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name">Name</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                
            </div>

             <div class="form-group">
                <label class="required" for="mobile-no">Description</label>
                <input class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" type="text" name="description" id="description" value="{{ old('description', '') }}" required>
                @if($errors->has('description'))
                    <div class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </div>
                @endif
               
            </div>

             <div class="form-group">
                <div class="input-field">
                  <select class="select2-size-sm browser-default" id="small-select" name="status">
                    <option value="1">Active</option>
                    <option value="2">Inactive</option>
                  </select>
                </div>           
            </div>

            <div class="form-group">
              
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Close</a>
  </div>
</div>