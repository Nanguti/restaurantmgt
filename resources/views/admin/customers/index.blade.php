@extends('admin.layouts.admin')
@section('content')
<div class="row">
    <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
      <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0"><span>Users List</span></h5>
                <ol class="breadcrumbs mb-0">
                  <li class="breadcrumb-item"><a href="index.html">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="#">User</a>
                  </li>
                  <li class="breadcrumb-item active">Users List
                  </li>
                </ol>
              </div>
              @can('user_create')
              <div class="col s2 m6 l6"><a href="#add-customer-modal" class="btn waves-effect waves-light breadcrumbs-btn right modal-trigger "><span class="hide-on-small-onl">{{ trans('global.add') }} Customer</span></a>                
              </div>
              @endcan
            </div>
          </div>
        </div>
        <div class="col s12">
        <div class="container">
        <!-- users list start -->
        <section class="users-list-wrapper section">
          <div class="users-list-filter">
            <div class="card-panel">
              <div class="row">
                <form>
                  <div class="col s12 m6 l3">
                    <label for="users-list-verified">Verified</label>
                    <div class="input-field">
                      <select class="form-control" id="users-list-verified">
                        <option value="">Any</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                      </select>
                    </div>
                  </div>
                  <div class="col s12 m6 l3">
                    <label for="users-list-role">Role</label>
                    <div class="input-field">
                      <select class="form-control" id="users-list-role">
                        <option value="">Any</option>
                        <option value="User">User</option>
                        <option value="Staff">Staff</option>
                      </select>
                    </div>
                  </div>
                  <div class="col s12 m6 l3">
                    <label for="users-list-status">Status</label>
                    <div class="input-field">
                      <select class="form-control" id="users-list-status">
                        <option value="">Any</option>
                        <option value="Active">Active</option>
                        <option value="Close">Close</option>
                        <option value="Banned">Banned</option>
                      </select>
                    </div>
                  </div>
                  <div class="col s12 m6 l3 display-flex align-items-center show-btn">
                    <button type="submit" class="btn btn-block indigo waves-effect waves-light">Show</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="users-list-table">
            <div class="card">
              <div class="card-content">
                <!-- datatable start -->
                <div class="responsive-table">
                  <table id="users-list-datatable" class="table">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            Id
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Mobile Number
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                           View
                        </th>
                        <th>Edit</th>
                        <th>Delete</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($customers as $key => $customer)
                        <tr data-entry-id="{{ $customer->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $customer->id ?? '' }}
                            </td>
                            <td>
                                {{ $customer->name ?? '' }}
                            </td>
                            <td>
                                {{ $customer->mobile_no ?? '' }}
                            </td>
                            <td>
                                {{ $customer->email ?? '' }}
                            </td>
                            <td>
                                @can('user_show')
    								<a class="waves-effect waves-light modal-trigger mb-2 mr-1" href="#details-modal" id="show-details" data-id="{{ $customer->id }}"><i class="material-icons">remove_red_eye</i></a>
                                @endcan
                            </td>
                            <td>

                                @can('user_edit')
                                    <a href="{{ route('customers.edit', $customer->id) }}">
                                        <i class="material-icons">edit</i>
                                    </a>
                                @endcan
                            </td>
                            <td>

                                @can('user_delete')
                                    <form action="{{ route('customers.destroy', $customer->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
                </table>
                </div>
                <!-- datatable ends -->
              </div>
            </div>
          </div>
        </section>
      </div>
      <div class="content-overlay"></div>
    </div>
  </div>
</div>
@endsection

<div id="add-customer-modal" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Add Customer</h4>
		<form method="POST" action="{{ route('customers.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name">Name</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                
            </div>

             <div class="form-group">
                <label class="required" for="mobile-no">Mobile</label>
                <input class="form-control {{ $errors->has('mobile_no') ? 'is-invalid' : '' }}" type="text" name="mobile_no" id="mobile_no" value="{{ old('mobile_no', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('mobile_no') }}
                    </div>
                @endif
               
            </div>

             <div class="form-group">
                <label class="required" for="name">Email</label>
                <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" id="email" value="{{ old('email', '') }}" required>
                @if($errors->has('email'))
                    <div class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </div>
                @endif
            
            </div>

             <div class="form-group">
                <label class="required" for="name">Address</label>
                <input class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" type="text" name="address" id="address" value="{{ old('address', '') }}" required>
                @if($errors->has('address'))
                    <div class="invalid-feedback">
                        {{ $errors->first('address') }}
                    </div>
                @endif
            </div>

            <div class="form-group">
            	
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Close</a>
	</div>
</div>


<div id="details-modal" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Customer Details</h4>
		<table class="table table-bordered table-striped">
	        <tbody>
	            <tr>
	                <td>
	                	Name
	                </td>

	                <td>
	                	<div id="customerName"></div>
	                </td>
	            </tr>
	            <tr>
	                <td>
	                	Mobile Number
	                </td>

	                <td>
	                	<div id="customerMobile"></div>
	                </td>
	            </tr>
	            <tr>
	                <td>
	                	Email
	                </td>

	                <td>
	                	<div id="customerEmail"></div>
	                </td>
	            </tr>
	            <tr>
	                <td>
	                	Address
	                </td>

	                <td>
	                	<div id="customerAddress"></div>
	                </td>
	            </tr>
	        </tbody>
	    </table>
	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Close</a>
	</div>
</div>

<script src="{{ asset('assets/js/vendors.min.js') }}"></script>
<script>
	$(document).ready(function () {
		$('body').on('click', '#show-details', function (event) {
		    event.preventDefault();
		    var id = $(this).data('id');
		    $.get('customers/' + id, function (data) {
		    	document.getElementById('customerName').innerHTML=data.customer.name;
		    	document.getElementById('customerMobile').innerHTML=data.customer.mobile_no;
		    	document.getElementById('customerEmail').innerHTML=data.customer.email;
		    	document.getElementById('customerAddress').innerHTML=data.customer.address;
		     })
		});
	}); 
</script>