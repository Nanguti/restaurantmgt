@extends('admin.layouts.admin')
@section('content')

<div class="row">
    <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
      <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0"><span>Users List</span></h5>
                <ol class="breadcrumbs mb-0">
                  <li class="breadcrumb-menu"><a href="index.html">Home</a>
                  </li>
                  <li class="breadcrumb-menu"><a href="#">User</a>
                  </li>
                  <li class="breadcrumb-menu active">Users List
                  </li>
                </ol>
              </div>
              @can('user_create')
              <div class="col s2 m6 l6"><a href="{{ route('users.create') }}" class="btn waves-effect waves-light breadcrumbs-btn right"><span class="hide-on-small-onl">{{ trans('global.add') }} {{ trans('cruds.user.title_singular') }}</span></a>                
              </div>
              @endcan
            </div>
          </div>
        </div>
        <div class="col s12">
        <div class="container">
        <!-- users list start -->
        <section class="users-list-wrapper section">
          <div class="users-list-filter">
            <div class="card-panel">
              <div class="row">
                <form>
                  <div class="col s12 m6 l3">
                    <label for="users-list-verified">Verified</label>
                    <div class="input-field">
                      <select class="form-control" id="users-list-verified">
                        <option value="">Any</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                      </select>
                    </div>
                  </div>
                  <div class="col s12 m6 l3">
                    <label for="users-list-role">Role</label>
                    <div class="input-field">
                      <select class="form-control" id="users-list-role">
                        <option value="">Any</option>
                        <option value="User">User</option>
                        <option value="Staff">Staff</option>
                      </select>
                    </div>
                  </div>
                  <div class="col s12 m6 l3">
                    <label for="users-list-status">Status</label>
                    <div class="input-field">
                      <select class="form-control" id="users-list-status">
                        <option value="">Any</option>
                        <option value="Active">Active</option>
                        <option value="Close">Close</option>
                        <option value="Banned">Banned</option>
                      </select>
                    </div>
                  </div>
                  <div class="col s12 m6 l3 display-flex align-menu-center show-btn">
                    <button type="submit" class="btn btn-block indigo waves-effect waves-light">Show</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="users-list-table">
            <div class="card">
              <div class="card-content">
                <!-- datatable start -->
                <div class="responsive-table">
                  <table id="users-list-datatable" class="table">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            Id
                        </th>
                        <th>
                            Customer Name
                        </th>
                        <th>
                          Description
                        </th>
                        <th>
                            Price
                        </th>
                        <th>
                            View
                        </th>
                        <th>
                            Edit
                        </th>
                         <th>
                            Delete
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $key => $order)
                        <tr data-entry-id="{{ $order->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $order->id ?? '' }}
                            </td>
                            <td>
                               {{ $order->customer->name }}
                            </td>
                            <td>
                                {{ $order->description ?? '' }}
                            </td>
                            <td>
                                {{ $order->amount }}
                            </td>
                            <td>
                                @can('menu_show')
                                   <a class="waves-effect waves-light modal-trigger mb-2 mr-1" href="#details-modal" id="show-details" data-id="{{ $order->id }}"><i class="material-icons">remove_red_eye</i></a>
                                @endcan
                              </td>
                              <td>

                                @can('menu_edit')
                                    <a href="{{ route('orders.edit', $order->id) }}">
                                        <i class="material-icons">edit</i>
                                    </a>
                                @endcan
                              </td>
                              <td>

                                @can('menu_delete')
                                    <form action="{{ route('orders.destroy', $order->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>
                        </tr>
                    @endforeach
                </tbody>
                </table>
                </div>
                <!-- datatable ends -->
              </div>
            </div>
          </div>
        </section>
      </div>
      <div class="content-overlay"></div>
    </div>
  </div>
</div>
@endsection

<div id="details-modal" class="modal modal-fixed-footer">
    <div class="modal-content">
        <div class="row">
            <div class="col s12 m6 l6">
                <h6>Customer Details</h6>
                <p>Name: <span id="customerName"></span></p>
                <p>Phone: <span id="customerPhone"></span></p>
                <p>Email: <span id="customerEmail"></span></p>
            </div>
            <div class="col s12 m6 l6">
                <h6>Customer Details</h6>
                <p>Order ID: #<span id="orderOrderId"></span></p>               
                <p>Order Status: <span id="orderStatus"></span></p>
                <p>Customer Address: <span id="customerAddress"></span></p>
                
            </div>
        </div>
        <div class="row order-items"></div>
        <hr>
        <div class="row">
            <div class="col s12 m8 l8">
                Total Amount
            </div>
            <div class="col s12 m4 l4">
                <div id="orderAmount"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Close</a>
    </div>
</div>

<script src="{{ asset('assets/js/vendors.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('body').on('click', '#show-details', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            $.get('orders/' + id, function (data) {
                var htmlOutput = '';
                var orderItems = data.orderItems;
                var customerDetails = data.customerDetails;
                var order =data.order;
                console.log(order);
                document.getElementById('orderAmount').innerHTML = order.amount;
                document.getElementById('orderStatus').innerHTML = order.status;
                document.getElementById('orderOrderId').innerHTML = order.id;
                document.getElementById('customerName').innerHTML = customerDetails.name;
                document.getElementById('customerPhone').innerHTML = customerDetails.mobile_no;
                document.getElementById('customerAddress').innerHTML = customerDetails.address;
                document.getElementById('customerEmail').innerHTML = customerDetails.email
                htmlOutput += '<div class="row"><div class="col s2 m4 l4">ID</div><div class="col s2 m4 l4">Item</div><div class="col s2 m4 l4">Price</div></div><hr>'
                orderItems.forEach(function (reply){
                    htmlOutput += '<div class="col s2 m4 l4">'+reply.id+'</div><div class="col s2 m4 l4">'+reply.name+'</div><div class="col s2 m4 l4">'+reply.price+'</div>'
                    
                });
                $('.order-items').html(htmlOutput);
             })
        });
    });




</script>