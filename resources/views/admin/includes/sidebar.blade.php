<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">
  <div class="brand-sidebar">
    <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="index.html"><img class="hide-on-med-and-down" src="{{ asset('assets/images/logo/materialize-logo-color.png') }}" alt="materialize logo"/><img class="show-on-medium-and-down hide-on-med-and-up" src="{{ asset('assets/images/logo/materialize-logo.png') }}" alt="materialize logo"/><span class="logo-text hide-on-med-and-down">Baiti Tech</span></a><a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
  </div>
  <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
    <li><a href="{{ route('dashboard') }}"><i class="material-icons">dashboard</i><span data-i18n="Second level">Dashboard</span></a>
          </li>

    <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="JavaScript:void(0)"><i class="material-icons">manage_accounts</i><span class="menu-title" data-i18n="Menu levels">User Management</span></a>
      <div class="collapsible-body">
        <ul class="collapsible collapsible-sub" data-collapsible="accordion">

          <li><a href="{{ route('permissions.index') }}"><i class="material-icons">add_task</i><span data-i18n="Second level">Permissions</span></a>
          </li>

          <li><a href="{{ route('roles.index')}}"><i class="material-icons">splitscreen</i><span data-i18n="Second level">Roles</span></a>
          </li>
          <li><a href="{{ route('branches.index') }}"><i class="material-icons">store</i><span data-i18n="Second level">Branches</span></a>
          </li>
          <li><a href="{{ route('users.index')}}"><i class="material-icons">person_add</i><span data-i18n="Second level">Users</span></a>
          </li>
        </ul>
      </div>
    </li>
    <li><a href="{{ route('transactions.index')}}"><i class="material-icons">receipt_long</i><span data-i18n="Second level">Transactions</span></a>
    </li>
    <li><a href="{{ route('stocks.index')}}"><i class="material-icons">inventory_2</i><span data-i18n="Second level">Stocks</span></a>
    </li>

    <li><a href="{{ route('customers.index')}}"><i class="material-icons">people_alt</i><span data-i18n="Second level">Customers</span></a>
    </li>
    <li><a href="{{ route('menu.index')}}"><i class="material-icons">menu_book</i><span data-i18n="Second level">Menu</span></a>
    
    <li><a href="{{ route('foodcategories.index')}}"><i class="material-icons">lunch_dining</i><span data-i18n="Second level">Food Categories</span></a>
    </li>
    </li>
    <li><a href="{{ route('orders.index')}}"><i class="material-icons">list_alt</i><span data-i18n="Second level">Orders</span></a>
    </li>
    </li>
  </ul>
  <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
