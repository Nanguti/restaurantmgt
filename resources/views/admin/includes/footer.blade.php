<footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
  <div class="footer-copyright">
    <div class="container"><span>&copy; {{ date('Y') }} <a href="#" target="_blank">BAITI</a> All rights reserved.</span><span class="right hide-on-small-only">Design and Developed by <a href="#">BAITI</a></span></div>
  </div>
</footer>

<!-- END: Footer-->
<!-- BEGIN VENDOR JS-->
<script src="{{ asset('assets/js/vendors.min.js') }}"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ asset('assets/vendors/chartjs/chart.min.js') }}"></script>
<script src="{{ asset('assets/vendors/chartist-js/chartist.min.js') }}"></script>
<script src="{{ asset('assets/vendors/chartist-js/chartist-plugin-tooltip.js') }}"></script>
<script src="{{ asset('assets/vendors/chartist-js/chartist-plugin-fill-donut.min.js') }}"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="{{ asset('assets/js/plugins.min.js') }}"></script>
<script src="{{ asset('assets/js/search.min.js') }}"></script>
<script src="{{ asset('assets/js/custom/custom-script.min.js') }}"></script>
<!-- <script src="{{ asset('assets/js/custom/custom-main.js') }}"></script> -->
<script src="{{ asset('assets/js/scripts/customizer.min.js') }}"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="{{ asset('assets/js/scripts/dashboard-modern.js') }}"></script>
<!-- <script src="{{ asset('assets/js/scripts/intro.min.js') }}"></script>
 --><script src="{{ asset('assets/js/scripts/advance-ui-modals.min.js') }}"></script>
<!-- END PAGE LEVEL JS-->

<!-- Begin of dataTables -->
<script src="{{ asset('assets/vendors/data-tables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/js/scripts/page-users.min.js') }}"></script>
<!-- End of dataTables -->