@extends('admin.layouts.admin')
@section('content')

<div class="row">
<div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
<div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
  <!-- Search for small screen-->
</div>
    <div class="col s12">
        <div class="container">
            <div class="seaction">
            <!-- Form with validation -->
                <div class="col s12 m12 l12">
                    <div id="form-with-validation" class="card card card-default scrollspy">
                        <div class="card-content">
                            <h4 class="card-title">Edit Menu</h4>
                                <form method="POST" action="{{ route('menu.update', [$menu->id]) }}" enctype="multipart/form-data">
                                    @method('PUT')
                                    @csrf
                                    <div class="form-group">
                                        <label class="required" for="name">Name</label>
                                        <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $menu->name) }}" required>
                                        @if($errors->has('name'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('name') }}
                                            </div>
                                        @endif
                                        
                                    </div>
                                    <div class="form-group">
                                        <label class="required" for="description">Mobile Number</label>
                                        <input class="form-control {{ $errors->has('mobile_no') ? 'is-invalid' : '' }}" type="text" name="description" id="description" value="{{ old('description', $menu->description) }}" required>
                                        @if($errors->has('description'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('description') }}
                                            </div>
                                        @endif
                                        
                                    </div>

                                    <div class="form-group">
                                        <label class="required" for="price">Price</label>
                                        <input class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" type="text" name="price" id="price" value="{{ old('price', $menu->price) }}" required>
                                        @if($errors->has('price'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('price') }}
                                            </div>
                                        @endif
                                        <span class="help-block">{{ trans('cruds.item.fields.name_helper') }}</span>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-field">
                                          <select class="select2-size-sm browser-default" id="small-select" name="food_category_id">
                                            <option value="1">BreakFast</option>
                                            <option value="2">Lunch</option>
                                            <option value="3">Supper</option>
                                            <option value="4">Anytime</option>
                                          </select>
                                        </div>           
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-danger" type="submit">
                                            {{ trans('global.save') }}
                                        </button>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="content-overlay"></div>
</div>
@endsection
