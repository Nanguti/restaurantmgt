@extends('admin.layouts.admin')
@section('content')

<div class="row">
<div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
<div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
  <!-- Search for small screen-->
</div>
    <div class="col s12">
        <div class="container">
            <div class="seaction">
            <!-- Form with validation -->
                <div class="col s12 m12 l12">
                    <div id="form-with-validation" class="card card card-default scrollspy">
                        <div class="card-content">
                            <h4 class="card-title">Item Details</h4>
                                <table class="table table-bordered table-striped">
                                    <tbody>
                                        <tr>
                                            <th>
                                                {{ trans('cruds.item.fields.id') }}
                                            </th>
                                            <td>
                                                {{ $item->id }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                {{ trans('cruds.item.fields.name') }}
                                            </th>
                                            <td>
                                                {{ $item->name }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                {{ trans('cruds.item.fields.description') }}
                                            </th>
                                            <td>
                                                {{ $item->description }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                Danger level
                                            </th>
                                            <td>
                                                {{ $item->danger_level }}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            <div class="form-group">
                                <a class="btn btn-default" href="{{ route('items.index') }}">
                                    {{ trans('global.back_to_list') }}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
