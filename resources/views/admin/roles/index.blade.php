@extends('admin.layouts.admin')
@section('content')

<div class="row">
    <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
      <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
          <!-- Search for small screen-->
          <div class="container">
            <div class="row">
              <div class="col s10 m6 l6">
                <h5 class="breadcrumbs-title mt-0 mb-0"><span>Users List</span></h5>
                <ol class="breadcrumbs mb-0">
                  <li class="breadcrumb-item"><a href="index.html">Home</a>
                  </li>
                  <li class="breadcrumb-item"><a href="#">User</a>
                  </li>
                  <li class="breadcrumb-item active">Users List
                  </li>
                </ol>
              </div>
              @can('role_create')
              <div class="col s2 m6 l6"><a href="{{ route('roles.create') }}" class="btn waves-effect waves-light breadcrumbs-btn right"><span class="hide-on-small-onl">{{ trans('global.add') }} {{ trans('cruds.role.title_singular') }}</span></a>                
              </div>
              @endcan
            </div>
          </div>
        </div>
        <div class="col s12">
        <div class="container">
        <!-- users list start -->
        <section class="users-list-wrapper section">
          <div class="users-list-filter">
            <div class="card-panel">
              <div class="row">
                <form>
                  <div class="col s12 m6 l3">
                    <label for="users-list-verified">Verified</label>
                    <div class="input-field">
                      <select class="form-control" id="users-list-verified">
                        <option value="">Any</option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                      </select>
                    </div>
                  </div>
                  <div class="col s12 m6 l3">
                    <label for="users-list-role">Role</label>
                    <div class="input-field">
                      <select class="form-control" id="users-list-role">
                        <option value="">Any</option>
                        <option value="User">User</option>
                        <option value="Staff">Staff</option>
                      </select>
                    </div>
                  </div>
                  <div class="col s12 m6 l3">
                    <label for="users-list-status">Status</label>
                    <div class="input-field">
                      <select class="form-control" id="users-list-status">
                        <option value="">Any</option>
                        <option value="Active">Active</option>
                        <option value="Close">Close</option>
                        <option value="Banned">Banned</option>
                      </select>
                    </div>
                  </div>
                  <div class="col s12 m6 l3 display-flex align-items-center show-btn">
                    <button type="submit" class="btn btn-block indigo waves-effect waves-light">Show</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="users-list-table">
            <div class="card">
              <div class="card-content">
                <!-- datatable start -->
                <div class="responsive-table">
                  <table id="users-list-datatable" class="table">
                    <thead>
                      <tr>
                        <th></th>
                        <th>id</th>
                        <th>Title</th>
                        <th>view</th>
                        <th>status</th>
                        <th>edit</th>
                        <th>delete</th>
                      </tr>
                    </thead>
                    <tbody>
                       @foreach($roles as $key => $role)
                      <tr>
                        <td></td>
                        <td>{{ $role->id ?? '' }}</td>
                        <td><a href="page-users-view.html">{{ $role->title ?? '' }}</a>
                        </td>
                        <td><a href="page-users-view.html"><i class="material-icons">remove_red_eye</i></a> </td>
                        <td><span class="chip green lighten-5">
                            <span class="green-text">Active</span>
                          </span>
                        </td>
                        <td>
                          @can('role_show')
                            <a href="{{ route('roles.edit', $role->id) }}">
                                <i class="material-icons">edit</i>
                            </a>
                          @endcan
                        <td>    
                          @can('role_delete')
                            <form action="{{ route('roles.destroy', $role->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                            </form>
                          @endcan
                        </td>
                      </tr>
                      @endforeach                  
                    </tbody>
                  </table>
                </div>
                <!-- datatable ends -->
              </div>
            </div>
          </div>
        </section>
      </div>
      <div class="content-overlay"></div>
    </div>
  </div>
</div>

@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('role_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('roles.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Role:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection