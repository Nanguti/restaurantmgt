@extends('admin.layouts.admin')
@section('content')

<div class="row">
<div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
<div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
  <!-- Search for small screen-->
</div>
    <div class="col s12">
        <div class="container">
            <div class="seaction">
            <!-- Form with validation -->
                <div class="col s12 m12 l12">
                    <div id="form-with-validation" class="card card card-default scrollspy">
                        <div class="card-content">
                            <h4 class="card-title">Add Permission</h4>
                            <form method="POST" action="{{ route('permissions.store') }}" enctype="multipart/form-data">@csrf
                            <div class="row">
                              <div class="input-field col m4 s6">
                                <i class="material-icons prefix">key</i>
                                <input class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title" id="title" value="{{ old('title', '') }}" required class="validate">
                                @if($errors->has('title'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('title') }}
                                    </div>
                                @endif
                                <!-- <input placeholder="John Doe" id="icon_prefix2" type="text" class="validate"> -->
                                <label for="icon_prefix2" class="active">{{ trans('cruds.permission.fields.title') }}</label>
                              </div>
                              <div class="input-field col m4 s12">
                                <div class="input-field col s12">
                                  <button class="btn cyan waves-effect waves-light" type="submit" name="action">
                                    <i class="material-icons left">perm_identity</i> {{ trans('global.save') }}</button>
                                </div>
                              </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection