<!doctype html>
<html>
<head>
   @include('admin.includes.head')
</head>
  <body>   
     <header class="page-topbar" id="header">
        @include('admin.includes.header')
     </header>
        @include('admin.includes.sidebar')

      <div id="main">
        <!--  -->
        @include('message.flash-message')
        @yield('content')
      </div>

     <footer>
         @include('admin.includes.footer')
     </footer>
  </body>
</html>