<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToTransactionsTable extends Migration
{
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->unsignedInteger('item_id');
            $table->foreign('item_id', 'item_fk_1230972')->references('id')->on('items');
            $table->unsignedInteger('branch_id')->nullable();
            $table->foreign('branch_id', 'branch_fk_1230977')->references('id')->on('branches');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id', 'user_fk_1233734')->references('id')->on('users');
        });

    }
}
