<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToStocksTable extends Migration
{
    public function up()
    {
        Schema::table('stocks', function (Blueprint $table) {
            $table->unsignedInteger('item_id');
            $table->foreign('item_id', 'item_fk_1230965')->references('id')->on('items');
            $table->unsignedInteger('branch_id')->nullable();
            $table->foreign('branch_id', 'branch_fk_1230970')->references('id')->on('branches');
        });

    }
}
