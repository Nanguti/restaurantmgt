<?php
namespace Database\Seeders;
use App\Models\Branch;
use App\Models\User;
use Illuminate\Database\Seeder;

class BranchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 2; $i++) {
            $randomNumber = rand(123, 789);

            $branch = Branch::factory()->create([
                'name' => "Branch $randomNumber",
            ]);

            $admin = User::factory()->create([
                'name'           => "Admin $randomNumber",
                'email'          => "admin$randomNumber@gmail.com",
                'password'       => bcrypt('password'),
                'branch_id'        => $branch->id,
                'remember_token' => null,
            ]);
            $admin->roles()->sync(2);

            $manager = User::factory()->create([
                'name'           => "Manager $randomNumber",
                'email'          => "manager$randomNumber@gmail.com",
                'password'       => bcrypt('password'),
                'branch_id'        => $branch->id,
                'remember_token' => null,
            ]);
            $manager->roles()->sync(3);
        }
    }
}
