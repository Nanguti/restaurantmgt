<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\BookTable;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BookTable::factory()->count(10)->create();
    }
}
