<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            PermissionsTableSeeder::class,
            RolesTableSeeder::class,
            PermissionRoleTableSeeder::class,
            UsersTableSeeder::class,
            RoleUserTableSeeder::class,
            BranchesTableSeeder::class,
            ItemsTableSeeder::class,
            CustomersSeeder::class,
            FoodCategorySeeder::class,
            MenuSeeder::class,
            OrdersSeeder::class,
            MenuOrderSeeder::class,
            BookTableSeeder::class
            

        ]);
    
    }
}
