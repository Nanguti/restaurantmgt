<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\FoodCategory;

class FoodCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $food_category = [
            'breakfast',
            'mid morning',
            'lunch',
            'supper',
            'late night',
            'snacks',
            'drinks'

        ];
        foreach ($food_category as $key => $value) {
            FoodCategory::factory()->create([
                'name'=>$value,
                'description'=>$value
            ]);
        }
    }
}
