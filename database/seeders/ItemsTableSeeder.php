<?php
namespace Database\Seeders;
use App\Models\Item;
use App\Models\Branch;
use Illuminate\Database\Seeder;

/**
 * Class AssetsTableSeeder
 */
class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $items = [
            'Cooking Oil',
            'Sugar',
            'Rice',
            'Maize Flour',
            'Wheat',
        ];

        foreach ($items as $item) {
            Item::factory()->create([
                'name'        => $item,
                'description' => $item
            ]);
        }
    }
}
