<?php

namespace Database\Seeders;
use App\Models\MenuOrder;
use Illuminate\Database\Seeder;

class MenuOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MenuOrder::factory()->count(10)->create();
    }
}
