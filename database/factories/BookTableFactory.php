<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\BookTable;

class BookTableFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = BookTable::class;
    public function definition()
    {
        return [
            'name'=>$this->faker->name(),
            'phone_number'=>$this->faker->e164PhoneNumber,
            'email'=>$this->faker->email,
            'message'=>$this->faker->text,
            'guest_number'=>$this->faker->randomDigit(),
            'status'=>$this->faker->randomElement(['pending', 'cancelled', 'approved']),
            'start_time'=>$this->faker->dateTimeBetween('-30 days', '+30 days'),
            'end_time'=>$this->faker->dateTimeBetween('-30 days', '+30 days'),
        ];
    }
}
