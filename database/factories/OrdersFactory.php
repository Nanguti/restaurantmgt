<?php

namespace Database\Factories;
use App\Models\Customers;
use App\Models\Orders;

use Illuminate\Database\Eloquent\Factories\Factory;

class OrdersFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Orders::class;
    public function definition()
    {
        return [
            'customer_id'=>Customers::inRandomOrder()->first()->id,
            'amount'=>$this->faker->numberBetween($min = 100, $max=5000),
            'status' =>$this->faker->randomElement(['pending', 'completed', 'processing', 'cancelled', 'rejected']),

        ];
    }
}
