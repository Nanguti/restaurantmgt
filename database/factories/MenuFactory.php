<?php

namespace Database\Factories;
//models
use App\Models\FoodCategory;
use App\Models\Menu;

use Illuminate\Database\Eloquent\Factories\Factory;

class MenuFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Menu::class;
    public function definition()
    {
        return [
            'food_category_id'=>FoodCategory::inRandomOrder()->first()->id,
            'name'=>$this->faker->name(),
            'image'=>'-'.$this->faker->randomDigit(),
            'description'=>$this->faker->text,
            'price'=>$this->faker->numberBetween($min =100, $max = 5000)
        ];
    }
}
