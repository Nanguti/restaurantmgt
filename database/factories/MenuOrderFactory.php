<?php

namespace Database\Factories;
use App\Models\Orders;
use App\Models\Menu;
use App\Models\MenuOrder;

use Illuminate\Database\Eloquent\Factories\Factory;

class MenuOrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model =  MenuOrder::class;
    public function definition()
    {
        return [
            'orders_id'=>Orders::inRandomOrder()->first()->id,
            'menu_id'=>Menu::inRandomOrder()->first()->id,
            'amount'=>$this->faker->numberBetween($min = 100, $max = 10000),
            'quantity' => $this->faker->randomDigit(),
        ];
    }
}
