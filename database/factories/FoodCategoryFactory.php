<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

//models
use App\Models\FoodCategory;

class FoodCategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = FoodCategory::class;
    public function definition()
    {
        return [
            'name'=>$this->faker->name(),
            'description'=>$this->faker->text,
            'status'=>$this->faker->randomDigit(),
        ];
    }
}
