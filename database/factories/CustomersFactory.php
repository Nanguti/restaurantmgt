<?php

namespace Database\Factories;
use App\Models\Customers;

use Illuminate\Database\Eloquent\Factories\Factory;

class CustomersFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Customers::class;

    public function definition()
    {
        return [
            'name'=>$this->faker->name(),
            'mobile_no'=>$this->faker->e164PhoneNumber,
            'address'=>$this->faker->address,
            'email'=>$this->faker->email,
        ];
    }
}
