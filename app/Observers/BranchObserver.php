<?php

namespace App\Observers;

use App\Models\Item;
use App\Models\Stock;
use App\Models\Branch;

/**
 * Class BranchObserver
 * @package App\Observers
 */
class BranchObserver
{
    /**
     * Handle the branch "created" event.
     *
     * @param  \App\Models\Branch  $branch
     * @return void
     */
    public function created(Branch $branch)
    {
        $items = Item::all();

        foreach ($items as $item) {
            Stock::factory()->create([
                'item_id' => $item->id,
                'branch_id'  => $branch->id,
            ]);
        }
    }

    /**
     * Handle the branch "updated" event.
     *
     * @param  \App\Models\Branch  $team
     * @return void
     */
    public function updated(Branch $branch)
    {
        //
    }

    /**
     * Handle the branch "deleted" event.
     *
     * @param  \App\Models\Branch  $barnch
     * @return void
     */
    public function deleted(Branch $branch)
    {
        //
    }

    /**
     * Handle the brench "restored" event.
     *
     * @param  \App\Model\Branch  $branch
     * @return void
     */
    public function restored(Branch $branch)
    {
        //
    }

    /**
     * Handle the branch "force deleted" event.
     *
     * @param  \App\Models\Branch  $branch
     * @return void
     */
    public function forceDeleted(Branch $barnch)
    {
        //
    }
}
