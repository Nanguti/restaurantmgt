<?php

namespace App\Observers;

use App\Models\Item;
use App\Models\Stock;
use App\Models\Branch;

/**
 * Class AssetObserver
 * @package App\Observers
 */
class ItemObserver
{
    /**
     * Handle the item "created" event.
     *
     * @param Item $item
     * @return void
     */
    public function created(Item $item)
    {
        $brnaches  = Branch::all();

        foreach ($brnaches as $branch) {
            Stock::factory()->create([
                'item_id' => $item->id,
                'branch_id'  => $branch->id,
            ]);
        }
    }

    /**
     * Handle the item "updated" event.
     *
     * @param Item $item
     * @return void
     */
    public function updated(Item $item)
    {
        //
    }

    /**
     * Handle the item "deleted" event.
     *
     * @param Item $item
     * @return void
     */
    public function deleted(Item $item)
    {
        //
    }

    /**
     * Handle the item "restored" event.
     *
     * @param Item $item
     * @return void
     */
    public function restored(Item $item)
    {
        //
    }

    /**
     * Handle the item "force deleted" event.
     *
     * @param Item $item
     * @return void
     */
    public function forceDeleted(Item $item)
    {
        //
    }
}
