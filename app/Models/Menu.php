<?php

namespace App\Models;
use App\Models\Orders;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    public function order()
    {
        return $this->belongsToMany(Orders::class);
    }

    protected $fillable = ['name', 'description','price','food_category_id'];
}
