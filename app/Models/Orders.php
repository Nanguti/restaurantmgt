<?php

namespace App\Models;
use App\Models\Menu;
use App\Models\Customers;
use App\Models\OrderMenu;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    use HasFactory;

    public function menuItems()
    {
        return $this->belongsToMany(Menu::class);
    }
    
    public function customer()
    {
        return $this->belongsTo(Customers::class);
    }
}
