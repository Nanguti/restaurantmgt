<?php

namespace App\Models;
use App\Models\Orders;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    use HasFactory;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'mobile_no',
        'email',
        'address',
        'created_at',
        'updated_at',    ];

    public function orders()
    {
        return $this->hasMany(Orders::class, 'customer_id');
    }
}
