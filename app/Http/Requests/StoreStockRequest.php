<?php

namespace App\Http\Requests;
use App\Models\Stock;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class StoreStockRequest
 * @package App\Http\Requests
 */
class StoreStockRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        abort_if(Gate::denies('stock_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'item_id'      => [
                'required',
                'integer',
                Rule::unique('stocks')
                    ->where('item_id', request()->input('item_id'))
                    ->where('branch_id', auth()->user()->branch_id)
                    ->whereNull('deleted_at'),
            ],
            'current_stock' => [
                'nullable',
                'integer',
            ],
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'item_id.unique' => 'The item is in stock already.',
        ];
    }
}
