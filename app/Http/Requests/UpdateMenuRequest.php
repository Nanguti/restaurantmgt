<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;
use Gate;
class UpdateMenuRequest extends FormRequest
{
   public function authorize()
    {
        abort_if(Gate::denies('menu_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'name'         => 'required',
            'food_category_id' => 'required|integer',
            'price' => 'integer|required',
            'description'=>'string|required'
        ];
    }
}
