<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;
use Gate;

class UpdateFoodCategoryRequest extends FormRequest
{
   public function authorize()
    {
        abort_if(Gate::denies('Edit Food Category'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'name'         => 'required',
            'description' => 'required|string',

        ];
    }}
