<?php

namespace App\Http\Requests;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreMenuRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('menu_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [

            'name'         => 'required',
            // 'price' => 'required|integer',
            // 'description' => 'string|required',
           // 'image'=>'required|string'

        ];
    }}
