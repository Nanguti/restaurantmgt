<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrdersRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('orders_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'amount'         => 'required',
            'customer_id' => 'required|integer',
            'status' => 'string|required',
        ];
    }
}
