<?php

namespace App\Http\Requests;

use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateCustomersRequest extends FormRequest
{
   public function authorize()
    {
        abort_if(Gate::denies('Edit Customer'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'name'      => 'required',
            'mobile_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:13',
            'address' => 'string|required',
            'email'=>'required|email'
        ];
    }
}
