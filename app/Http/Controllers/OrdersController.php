<?php

namespace App\Http\Controllers;

use App\Models\Orders;
use App\Http\Requests\StoreOrdersRequest;
use App\Http\Requests\UpdateOrdersRequest;
//Gate
use Gate;

use Symfony\Component\HttpFoundation\Response;

class OrdersController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('Access Orders'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $orders = Orders::all();

        return view('admin.orders.index', compact('orders'));
    }

    public function create()
    {
        abort_if(Gate::denies('Create Order'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.orders.create');
    }

    public function store(StoreOrdersRequest $request)
    {
        $orders = Orders::create($request->all());

        return redirect()->route('orders.index');

    }

    public function edit(Orders $orders)
    {
        abort_if(Gate::denies('Edit Order'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.orders.edit', compact('orders'));
    }

    public function update(UpdateOrdersRequest $request, Orders $orders)
    {
        $orders->update($request->all());

        return redirect()->route('orders.index');

    }

    public function show(Orders $order)
    {
        abort_if(Gate::denies('Show Order'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return response()->json([
            'order'=>$order,
            'customerDetails'=>$order->customer,
            'orderItems'=>$order->menuItems
        ]);

    }

    public function destroy(Orders $orders)
    {
        abort_if(Gate::denies('order_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $menu->delete();

        return back();

    }

    public function massDestroy(MassDestroyOrdersRequest $request)
    {
        Orders::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }}
