<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $menu = Menu::get();

        return view('frontend.index', ['menu'=>$menu]);
    }

    public function menu()
    {
        $menu = Menu::get();
        return view('frontend.menu', ['menu'=>$menu]);
    }
    public function aboutus()
    {
        return view('frontend.about-us');
    }

    public function services()
    {
        return view('frontend.services');
    }
    public function contact()
    {
        return view('frontend.contact');
    }



}
