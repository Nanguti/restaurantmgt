<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

//models
use App\Models\Customers;
use App\Models\Menu;
use App\Models\Orders;

//requests
use App\Http\Requests\StoreCustomersRequest;
use App\Http\Requests\UpdateCustomersRequest;

use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomersController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('customer_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $customers = Customers::all();

        return view('admin.customers.index', compact('customers'));
    }

    public function create()
    {
        abort_if(Gate::denies('customer_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.customers.create');
    }

    public function store(StoreCustomersRequest $request)
    {
       
        $customer = Customers::create($request->all());

        return redirect()->route('customers.index');

    }

    public function edit(Customers $customer)
    {
        abort_if(Gate::denies('Edit Customer'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.customers.edit', compact('customer'));
    }

    public function update(UpdateCustomersRequest $request, Customers $customer)
    {
        
        $customer->update($request->all());

        return redirect()->route('customers.index');

    }

    public function show(Customers $customer)
    {
        abort_if(Gate::denies('customer_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return response()->json(['customer'=>$customer]);
    }

    public function destroy(Customers $customer)
    {
        abort_if(Gate::denies('customer_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $customer->delete();

        return back();

    }

    public function massDestroy(MassDestroyCustomerRequest $request)
    {
        Customer::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

}
