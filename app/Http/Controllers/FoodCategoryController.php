<?php

namespace App\Http\Controllers;

use App\Models\FoodCategory;
use App\Http\Requests\StoreFoodCategoryRequest;
use App\Http\Requests\UpdateFoodCategoryRequest;

//Gate
use Gate;

use Symfony\Component\HttpFoundation\Response;

class FoodCategoryController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('food category access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $food_categories = FoodCategory::all();
        return view('admin.foodcategories.index', compact('food_categories'));

    }

    public function create()
    {
        abort_if(Gate::denies('food_category_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.foodcategories.create');
    }

    public function store(StoreFoodCategoryRequest $request)
    {
        $food_category = FoodCategory::create($request->all());

        return redirect()->route('foodcategories.index');

    }

    public function edit(FoodCategory $foodcategory)
    {

        abort_if(Gate::denies('Edit Food Category'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.foodcategories.edit', compact('foodcategory'));
    }

    public function update(UpdateFoodCategoryRequest $request, FoodCategory $foodcategory)
    {
        $foodcategory->update($request->all());

        return redirect()->route('foodcategories.index');

    }

    public function show(FoodCategory $foodcategory)
    {
        abort_if(Gate::denies('food_category_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.foodcategories.show', compact('foodcategory'));
    }

    public function destroy(FoodCategory $foodcategory)
    {
        abort_if(Gate::denies('Delete Food Category'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $foodcategory->delete();

        return back();

    }

    public function massDestroy(MassDestroyFoodCategoryRequest $request)
    {
        FoodCategory::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

}
